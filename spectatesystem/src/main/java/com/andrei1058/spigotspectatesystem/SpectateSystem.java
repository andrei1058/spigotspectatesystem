package com.andrei1058.spigotspectatesystem;

import org.bukkit.plugin.Plugin;

public class SpectateSystem {

    private static SpectateSystem spectateSystem;

    private SpectateSystem(Plugin plugin) {

    }

    /**
     * Initialize spectate system.
     *
     * @param plugin - plugin instance.
     * @return class instance. Will return instance if already initialized.
     */
    public static SpectateSystem init(Plugin plugin) {
        if (spectateSystem != null) return spectateSystem;
        return spectateSystem = new SpectateSystem(plugin);
    }
}
